//
//  UIImageView+AsynImageDownload.swift
//  NewsApp
//
//  Created by pushpender on 24/07/18.
//  Copyright © 2018 UE. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
