//
//  NewsCell.swift
//  NewsApp
//
//  Created by pushpender on 21/07/18.
//  Copyright © 2018 UE. All rights reserved.
//

import UIKit

let cornerRadius = CGFloat(7.5)
let borderColor = UIColor.lightGray.cgColor
let borderWidth = CGFloat(1.0)

class NewsCell: UITableViewCell {
    @IBOutlet weak var publisherImgView: UIImageView!
    @IBOutlet weak var articleImgView: UIImageView!
    @IBOutlet weak var publisherNameLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var keywordLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    
    static let NEWSCELLIDENTIFIER = "NEWSCELLIDENTIFIER"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dateTimeLbl.layer.cornerRadius = cornerRadius
        dateTimeLbl.layer.borderColor = borderColor
        dateTimeLbl.layer.borderWidth = borderWidth
        dateTimeLbl.clipsToBounds = true
        
        keywordLbl.layer.cornerRadius = cornerRadius
        keywordLbl.layer.borderColor = borderColor
        keywordLbl.layer.borderWidth = borderWidth
        keywordLbl.clipsToBounds = true
        
        dateTimeLbl.text = "  6 mins and 6 days  "
        keywordLbl.text = "  International, Lifestyle  "
        titleLbl.text = "The resurrection of Nantes: how free public art brought the city back to life"
        descriptionLbl.text = "After a carousel ride abroad a giant octopus with moving tentacles, we merge to find an oversized mechanical elephant wandering down the street, steam blowing from its trunk."
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
