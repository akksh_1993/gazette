//
//  AppDelegate.swift
//  NewsApp
//
//  Created by pushpender on 17/07/18.
//  Copyright © 2018 UE. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabBarController:UITabBarController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.tabBarController = UITabBarController()
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeVC")
        homeViewController.tabBarItem = UITabBarItem(title: "Home", image: nil, tag: 0)
        homeViewController.title = NSLocalizedString("Home", comment: "")
        
        let trendingViewController = mainStoryBoard.instantiateViewController(withIdentifier: "TrendingVC")
        trendingViewController.tabBarItem = UITabBarItem(title: "Trending", image: nil, tag: 1)
        trendingViewController.title = NSLocalizedString("Trending", comment: "")
        
        let searchViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SearchVC")
        searchViewController.tabBarItem = UITabBarItem(title: "Search", image: nil, tag: 2)
        searchViewController.title = NSLocalizedString("Search", comment: "")
        
        let exploreViewController = mainStoryBoard.instantiateViewController(withIdentifier: "ExploreVC")
        exploreViewController.tabBarItem = UITabBarItem(title: "Explore", image: nil, tag: 3)
       exploreViewController.title = NSLocalizedString("Explore", comment: "")
        
        let moreViewController = mainStoryBoard.instantiateViewController(withIdentifier: "MoreVC")
        moreViewController.tabBarItem = UITabBarItem(title: "More", image: nil, tag: 4)
        moreViewController.title = NSLocalizedString("More", comment: "")
        
        let controllers = [homeViewController,trendingViewController,searchViewController,exploreViewController,moreViewController]
        
        tabBarController?.viewControllers = controllers
        
        tabBarController?.viewControllers = controllers.map( {UINavigationController(rootViewController: $0) })
        self.window?.rootViewController = tabBarController
        

        self.window?.makeKeyAndVisible()
        
        
        //home
        //trending
        //search
        //explore
        //more
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    class func sharedAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func showAlertMessage(title:String,message:String)->UIAlertController{
        
        let localizedTitle = NSLocalizedString(title, comment: "")
        let localizedMessage = NSLocalizedString(message, comment: "")
        let alert = UIAlertController(title: localizedTitle, message: localizedMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        return alert
        
    }
    

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

