//
//  HomeVC.swift
//  NewsApp
//
//  Created by pushpender on 21/07/18.
//  Copyright © 2018 UE. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: UIViewController {

    @IBOutlet weak var newTableView: UITableView!
    var newsData = [NewsData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewSetup()
        self.getNewsList()
        // Do any additional setup after loading the view.
    }
    
    private func tableViewSetup() {
        self.newTableView.dataSource = self
        self.newTableView.delegate = self
        self.newTableView.register(UINib(nibName: "NewsCell", bundle: nil) , forCellReuseIdentifier: "NEWSCELLIDENTIFIER")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getNewsList() {
        
        let parameters:[String:Any] = [
            "user_id" : "5b44aef9e9ef9f4822dfd671",
            "pageNo" : 1,
            "categories" : "5afe827185bee95132c9d46b,5afe91d9188f295178acbb00",
            "publications" : "5afe91d9188f295178acbb07,5b0818cb188f295178acbb15"
        ]
        
//        let header = [
//            "Content-Type": "application/x-www-form-urlencoded"
//        ]
        
        Alamofire.request(UrlCreater.getNewsListUrl(), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
              guard response.result.isSuccess  else{
                print("error not response with error - \(String(describing: response.result.error))")
                if let err = response.result.error as? URLError, err.code  == URLError.Code.notConnectedToInternet
                {
                    self.present(AppDelegate.sharedAppDelegate().showAlertMessage(title: NSLocalizedString("No Internet Connection", comment: ""), message: NSLocalizedString("Make_sure_your_device_is_connected_to_the_Internet", comment: "")), animated: true, completion: nil)
                }
                return
            }
            
            guard let responseJSON = response.result.value as? [String: AnyObject] else
            {
                print("not valid response")
                return
            }
            
          //  var responseDict = response as! NSDictionary
            let newsData:NewsList = NewsList(dictionary: responseJSON as NSDictionary)!
            print(newsData.data)
            self.newsData = newsData.data!
            self.newTableView.reloadData()
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.s
    }
    */

}

extension HomeVC:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //
        let cell = tableView.dequeueReusableCell(withIdentifier: "NEWSCELLIDENTIFIER")! as! NewsCell
        let newsListObj = self.newsData[indexPath.row]
        
        if let publicationUrl =  newsListObj.publicationIcon {
            let url = URL(string: publicationUrl)
            cell.publisherImgView.load(url: url!)
        }
        
        if let articleUrl = newsListObj.filepreview {
            let url = URL(string: articleUrl)
            cell.articleImgView.load(url: url!)
        }
        
        cell.publisherNameLbl.text = newsListObj.publicationName
        cell.titleLbl.text = newsListObj.title
        cell.descriptionLbl.text = newsListObj.description
        let updatedTime = "  " + newsListObj.uploadTime! + "  "
        cell.dateTimeLbl.text = updatedTime
        cell.categoryLabel.text = newsListObj.categoryName
        let image = newsListObj.bookmark! ? #imageLiteral(resourceName: "bookmarked") : #imageLiteral(resourceName: "bookmark")
        cell.bookmarkButton.setImage(image, for: .normal)
        
        return cell
    }
    
}

extension HomeVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.newTableView.deselectRow(at: indexPath, animated: true)
        
        let newsListObj = self.newsData[indexPath.row]
        if let newsLink = newsListObj.link {
             let wkWebViewController = ArticleDetailVC(newsLink: newsLink)
            let homeNavCotroller:UINavigationController = AppDelegate.sharedAppDelegate().tabBarController?.viewControllers![0] as! UINavigationController
            homeNavCotroller.pushViewController(wkWebViewController, animated: true)
        }
       
    }
}
