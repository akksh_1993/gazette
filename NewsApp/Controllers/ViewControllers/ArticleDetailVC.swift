//
//  ArticleDetailVC.swift
//  NewsApp
//
//  Created by pushpender on 26/07/18.
//  Copyright © 2018 UE. All rights reserved.
//

import UIKit
import WebKit
class ArticleDetailVC: UIViewController {
    
    var webView: WKWebView!
    var newsLink: String?
    
    init(newsLink:String) {
        super.init(nibName: nil, bundle: nil)
        self.newsLink = newsLink
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func loadView() {
        webView = WKWebView()
       // webView.navigationDelegate = self
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://www.hackingwithswift.com")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //loadhtm

}

//class ArticleDetailVC: WKNavigationDelegate {
//
//}

