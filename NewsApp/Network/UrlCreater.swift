//
//  UrlCreater.swift
//  NewsApp
//
//  Created by pushpender on 21/07/18.
//  Copyright © 2018 UE. All rights reserved.
//

import UIKit
let SERVER_URL = "http://gazetteapp.net/api/"

class UrlCreater: NSObject {
    
    
//    static  public func getHeader() -> [:] {
//        let header = [
//            "Content-Type": "application/x-www-form-urlencoded"
//        ]
//    }
    
    static  public func getNewsListUrl() -> String {
        
        let urlString: String = SERVER_URL + "v1/mobile/posts/homeScreenPost"
        return urlString
    }
    
    
    static public func getTripsBy(_ mobileUserId: String, isPreviousTrip: Bool, date:String?) -> String {
        
        let isPreviousTripValueStringValue = isPreviousTrip ? "true" : "false"
        var urlString: String = SERVER_URL + "Rest/api/GetTrips?mobileUserId=" + mobileUserId + "&isPreviousTrip="
        urlString = urlString + isPreviousTripValueStringValue + "&date=" + date!
        return urlString
    }

}
