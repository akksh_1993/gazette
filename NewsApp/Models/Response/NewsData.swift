/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class NewsData {
	public var id : String?
	public var title : String?
	public var description : String?
	public var keywordword : Array<String>?
	public var publicationName : String?
	public var author : String?
	public var filepreview : String?
	public var link : String?
	public var publishedAt : String?
	public var uploadTime : String?
	public var publicationIcon : String?
	public var categoryName : String?
	public var viewCount : Int?
	public var bookmark : Bool?
	public var country : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let data_list = Data.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Data Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [NewsData]
    {
        var models:[NewsData] = []
        for item in array
        {
            models.append(NewsData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let data = Data(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Data Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? String
		title = dictionary["title"] as? String
		description = dictionary["description"] as? String
		//if (dictionary["keyword"] != nil) { keyword = Keyword.modelsFromDictionaryArray(dictionary["keyword"] as! NSArray) }
		publicationName = dictionary["publicationName"] as? String
		author = dictionary["author"] as? String
		filepreview = dictionary["filepreview"] as? String
		link = dictionary["link"] as? String
		publishedAt = dictionary["publishedAt"] as? String
		uploadTime = dictionary["uploadTime"] as? String
		publicationIcon = dictionary["publicationIcon"] as? String
		categoryName = dictionary["categoryName"] as? String
		viewCount = dictionary["viewCount"] as? Int
		bookmark = dictionary["bookmark"] as? Bool
		country = dictionary["country"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.description, forKey: "description")
		dictionary.setValue(self.publicationName, forKey: "publicationName")
		dictionary.setValue(self.author, forKey: "author")
		dictionary.setValue(self.filepreview, forKey: "filepreview")
		dictionary.setValue(self.link, forKey: "link")
		dictionary.setValue(self.publishedAt, forKey: "publishedAt")
		dictionary.setValue(self.uploadTime, forKey: "uploadTime")
		dictionary.setValue(self.publicationIcon, forKey: "publicationIcon")
		dictionary.setValue(self.categoryName, forKey: "categoryName")
		dictionary.setValue(self.viewCount, forKey: "viewCount")
		dictionary.setValue(self.bookmark, forKey: "bookmark")
		dictionary.setValue(self.country, forKey: "country")

		return dictionary
	}

}
